from collections import defaultdict

from jinja2 import Environment, FileSystemLoader


def get_inverted_index_dict(words_file):
    inverted_index = defaultdict(lambda: defaultdict(int))
    current_document_id = 1
    with open(words_file) as f:
        for line in f:
            if line != '\n':
                for word in line.split():
                    inverted_index[word][current_document_id] += 1
            else:
                current_document_id += 1
    return inverted_index


def save_inverted_index(inverted_index_dict, template_path, destintion_path, transform_internal_dicts_to_tuples=True):
    env = Environment(loader=FileSystemLoader(''))
    template = env.get_template(template_path)
    inverted_index_tuples = [(k, v.items() if transform_internal_dicts_to_tuples else v)
                             for (k, v) in inverted_index_dict.items()]
    inverted_index_tuples.sort(key=lambda i: i[0])
    output_from_parsed_template = template.render(term_list=inverted_index_tuples)
    with open(destintion_path, 'w') as fh:
        fh.write(output_from_parsed_template)


def main():
    inverted_index_dict = get_inverted_index_dict('../t02_preprocessing/result.txt')
    save_inverted_index(inverted_index_dict, 'inverted_index_result.xml.j2', 'inverted_index_result.xml')


if __name__ == '__main__':
    main()

	# зачтено