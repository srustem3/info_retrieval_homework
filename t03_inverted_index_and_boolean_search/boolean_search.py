import os

from bs4 import BeautifulSoup

from t02_preprocessing.preprocess import process_line


def load_inverted_index(file_path):
    inverted_index = {}
    with open(file_path) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        term_list = soup.find_all('term')
        for term in term_list:
            doc_list = []
            for doc in term.find_all('doc'):
                doc_list.append((int(doc['id']), int(doc['count'])))
            inverted_index[term['value']] = doc_list
    return inverted_index


def get_count_of_docs_for_word_wrapper(all_docs_ids, inverted_index):
    def internal(word):
        count_of_docs = len([t[0] for t in inverted_index.get(word.strip('-'), [])])
        if word[0] == '-':
            return len(all_docs_ids) - count_of_docs
        return count_of_docs
    return internal


def get_all_docs_ids(inverted_index):
    res = set()
    sets = [{t[0] for t in v} for k, v in inverted_index.items()]
    for s in sets:
        res |= s
    return res


def get_docs_ids(inverted_index, word):
    return {t[0] for t in inverted_index.get(word.strip('-'), [])}


def process_query(query_string, inverted_index):
    all_docs_ids = get_all_docs_ids(inverted_index)
    sorted_words = sorted(query_string.split(), key=get_count_of_docs_for_word_wrapper(all_docs_ids, inverted_index))
    intersection_result = all_docs_ids

    for word in sorted_words:
        docs_ids_of_current_word = get_docs_ids(inverted_index, word)
        if word.startswith('-'):
            intersection_result = intersection_result - docs_ids_of_current_word
        else:
            intersection_result &= docs_ids_of_current_word

    return intersection_result


def get_title_and_url_of_docs(xml_file_name, id_list):
    result = []
    with open(xml_file_name) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        for doc_id in id_list:
            docs = soup.find_all('document', {'id': doc_id})
            for doc in docs:
                result.append((doc.find_all('title')[0].text, doc.find_all('url')[0].text))
    return result


def save_titles_and_urls(query, titles_and_urls, destination_path):
    with open(destination_path, 'a') as file:
        file.write(query + os.linesep)
        for title, url in titles_and_urls:
            file.write(f'{title} - {url}{os.linesep}')
        file.write(os.linesep)


def normalize_query_list(query_list):
    return [process_line(query, valid_symbols_regexp='[^а-я- ]').replace('- ', '-') for query in query_list]


def main():
    boolean_search_result_file = 'boolean_search_result.txt'
    inverted_index_result_file = 'inverted_index_result.xml'
    web_crawler_result_file = '../t01_web_crawler/result.xml'

    query_list = ['по -базовому трафику в подсети',
                  'базовые станции билайна',
                  'безопасность -современной версии браузера',
                  'контроль версий -история',
                  'роскомнадзор разблокировал -рутрекер',
                  'экзамен по информационному поиску']
    normalized_query_list = normalize_query_list(query_list)

    inverted_index = load_inverted_index(inverted_index_result_file)

    if os.path.exists(boolean_search_result_file):
        os.remove(boolean_search_result_file)

    for query, normalized_query in zip(query_list, normalized_query_list):
        print(query)
        print(normalized_query)
        docs_ids = process_query(normalized_query, inverted_index)
        titles_and_urls = get_title_and_url_of_docs(web_crawler_result_file, docs_ids)
        save_titles_and_urls(query, titles_and_urls, boolean_search_result_file)


if __name__ == '__main__':
    main()

	
	# зачтено