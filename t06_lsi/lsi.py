import os

import numpy as np

from t02_preprocessing.preprocess import process_line
from t03_inverted_index_and_boolean_search.boolean_search import get_all_docs_ids
from t05_vector_model.vector_model import load_inverted_index_with_tfidf_extended, get_doc_term_matrix, \
    get_urls_of_docs, normalize_vector


def calculate_transformed_docs_vectors(matrix, u, s):
    result = []
    for doc in matrix.T:
        result.append(doc.dot(u).dot(np.linalg.inv(s)))
    return result


def get_query_term_vector(query, inverted_index):
    result = dict.fromkeys(inverted_index.keys(), 0)
    unique_query_words = set(query.split())
    for word in unique_query_words:
        if word in result:
            result[word] = 1
    return np.array([val for val in result.values()])


def get_cos_between_vectors(query_vector, doc_vector):
    dot_product = sum([q * d for q, d in zip(query_vector, doc_vector)])
    return dot_product / (normalize_vector(query_vector) * normalize_vector(doc_vector))


def get_ranked_by_lsi_results(query, inverted_index, transformed_docs_vectors, u, s, first_results_count=10):
    query_vector = get_query_term_vector(query, inverted_index)
    transformed_query_vector = query_vector.dot(u).dot(np.linalg.inv(s))
    cos_lst = [get_cos_between_vectors(transformed_query_vector, transformed_doc_vector)
               for transformed_doc_vector in transformed_docs_vectors]

    sorted_docs = sorted(enumerate(cos_lst), key=lambda el: el[1], reverse=True)
    sorted_docs = [(doc_id + 1, cos) for doc_id, cos in sorted_docs]
    return sorted_docs[:first_results_count]


def get_transformed_doc_term_matrix(doc_term_matrix):
    doc_term_matrix = [[val for val in doc.values()] for doc in doc_term_matrix]
    return np.array(doc_term_matrix).T


def get_reduced_svd_components(transformed_doc_term_matrix, k):
    u, s, vh = np.linalg.svd(transformed_doc_term_matrix, full_matrices=False)
    u = u[:, :k]
    s = np.diag(s)[:k, :k]
    vh = vh[:k, :]
    return u, s, vh


def main():
    k = 5
    inverted_index_extended_path = '../t05_vector_model/inverted_index_with_tfidf_extended_result.xml'
    xml_file_path = '../t01_web_crawler/result.xml'
    lsi_result_path = 'lsi_result.txt'

    inverted_index_with_tfidf_and_idf = load_inverted_index_with_tfidf_extended(inverted_index_extended_path)

    doc_term_matrix = get_doc_term_matrix(inverted_index_with_tfidf_and_idf)
    transformed_doc_term_matrix = get_transformed_doc_term_matrix(doc_term_matrix)

    u, s, vh = get_reduced_svd_components(transformed_doc_term_matrix, k)
    transformed_docs_vectors = calculate_transformed_docs_vectors(transformed_doc_term_matrix, u, s)

    query_list = ['по базовому трафику в подсети',
                  'базовые станции билайна',
                  'безопасность современной версии браузера',
                  'контроль версий история',
                  'роскомнадзор разблокировал рутрекер',
                  'экзамен по информационному поиску']
    normalized_query_list = [process_line(query) for query in query_list]

    all_docs_ids = sorted(get_all_docs_ids(inverted_index_with_tfidf_and_idf))
    url_by_id_dict = get_urls_of_docs(xml_file_path, all_docs_ids)

    with open(lsi_result_path, 'w') as lsi_result_file:
        for query, normalized_query in zip(query_list, normalized_query_list):
            lsi_result_file.write(query + os.linesep)

            res = get_ranked_by_lsi_results(normalized_query, inverted_index_with_tfidf_and_idf,
                                            transformed_docs_vectors, u, s)
            for doc_id, cos in res:
                lsi_result_file.write(f'{url_by_id_dict[doc_id]} - {cos}{os.linesep}')
            lsi_result_file.write(os.linesep)


if __name__ == '__main__':
    main()
