import os

from bs4 import BeautifulSoup

from t03_inverted_index_and_boolean_search.boolean_search import normalize_query_list, process_query, \
    save_titles_and_urls, get_title_and_url_of_docs


def load_inverted_index_with_tfidf(file_path):
    inverted_index = {}
    with open(file_path) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        term_list = soup.find_all('term')
        for term in term_list:
            doc_list = []
            for doc in term.find_all('doc'):
                doc_list.append([
                    int(doc['id']),
                    int(doc['count']),
                    float(doc['tf-idf'])
                ])
            inverted_index[term['value']] = doc_list
    return inverted_index


def get_score_wrapper(query_list_without_inverted_signs, inverted_index_with_tfidf):
    def internal(doc_id):
        tfidf_sum = 0.0
        for word in query_list_without_inverted_signs:
            for doc in inverted_index_with_tfidf.get(word, [[doc_id, 0, 0]]):
                if doc[0] == doc_id:
                    tfidf_sum += doc[2]
        return tfidf_sum
    return internal


def rank_query(query_string, inverted_index_with_tfidf, docs_ids):
    query_list_without_inverted_signs = []
    for word in query_string.split():
        if not word.startswith('-'):
            query_list_without_inverted_signs.append(word)
    return sorted(docs_ids,
                  key=get_score_wrapper(query_list_without_inverted_signs, inverted_index_with_tfidf),
                  reverse=True)


def main():
    ranked_boolean_search_result_file = 'ranked_boolean_search_result.txt'
    inverted_index_with_tfidf_result_file = 'inverted_index_with_tfidf_result.xml'
    web_crawler_result_file = '../t01_web_crawler/result.xml'

    query_list = ['по -базовому трафику в подсети',
                  'базовые станции билайна',
                  'безопасность -современной версии браузера',
                  'контроль версий -история',
                  'роскомнадзор разблокировал -рутрекер',
                  'экзамен по информационному поиску']
    normalized_query_list = normalize_query_list(query_list)

    inverted_index_with_tfidf = load_inverted_index_with_tfidf(inverted_index_with_tfidf_result_file)

    if os.path.exists(ranked_boolean_search_result_file):
        os.remove(ranked_boolean_search_result_file)

    for query, normalized_query in zip(query_list, normalized_query_list):
        print(query)
        print(normalized_query)
        docs_ids = process_query(normalized_query, inverted_index_with_tfidf)
        docs_ids = rank_query(normalized_query, inverted_index_with_tfidf, docs_ids)
        titles_and_urls = get_title_and_url_of_docs(web_crawler_result_file, docs_ids)
        save_titles_and_urls(query, titles_and_urls, ranked_boolean_search_result_file)


if __name__ == '__main__':
    main()
