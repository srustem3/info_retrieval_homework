dataclasses import dataclass

from bs4 import BeautifulSoup
import requests

from jinja2 import Environment, FileSystemLoader


pages_to_parse = [
    'https://habr.com/ru/news/',
    'https://habr.com/ru/news/page2/'
]


@dataclass
class HabrPage:
    url: str
    title: str
    text: str
    keywords: str


def get_page_html(link):
    request_result = requests.get(link)
    return request_result.text


def get_links_from_habr_page(habr_page_link):
    main_page_html = get_page_html(habr_page_link)

    soup = BeautifulSoup(main_page_html, 'html.parser')

    link_list = []
    for link in soup.find_all('a', class_='post__title_link'):
        link_list.append(link.get('href'))

    return link_list


def get_title(html_page_soup):
    return html_page_soup.find_all('span', class_='post__title-text')[0].get_text()


def get_tags(html_page_soup):
    return [tag.get_text() for tag in html_page_soup.find_all('a', class_='inline-list__item-link hub-link')]


def get_text(html_page_soup):
    return html_page_soup.find_all('div', class_='post__body post__body_full')[0].get_text()


def get_parsed_pages():
    links = [link for habr_page in pages_to_parse for link in get_links_from_habr_page(habr_page)][:30]
    habr_pages = []

    for i, link in enumerate(links):
        text = get_page_html(link)
        soup = BeautifulSoup(text, 'html.parser')

        title = get_title(soup)
        tags = ','.join(get_tags(soup))
        text = ''.join([line.strip() for line in get_text(soup).split('\n')])

        habr_pages.append(HabrPage(url=link, title=title, text=text, keywords=tags))

    return habr_pages


def save_parsed_pages(parsed_pages):
    env = Environment(loader=FileSystemLoader(''))
    template = env.get_template('result.xml.j2')
    output_from_parsed_template = template.render(pages=enumerate(parsed_pages))
    with open('result.xml', 'w') as fh:
        fh.write(output_from_parsed_template)


def main():
    habr_pages = get_parsed_pages()
    save_parsed_pages(habr_pages)


if __name__ == '__main__':
    main()

	# зачтено