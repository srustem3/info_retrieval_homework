import os
import re
from dataclasses import dataclass

from bs4 import BeautifulSoup
from pymystem3 import Mystem


@dataclass
class PageInfo:
    title: str
    text: str


def delete_stop_words(text):
    stop_words_list = []
    with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'stopwords.txt'), 'r') as stop_words_file:
        for line in stop_words_file:
            stop_words_list.append(line[:-1])
    return ' '.join([word for word in text.split() if word not in stop_words_list])


def delete_extra_symbols(text, valid_symbols_regexp):
    regex = re.compile(valid_symbols_regexp)
    return ' '.join([word.strip() for word in regex.sub(' ', text).split()])


def lemmatize(text):
    m = Mystem()
    lemmas = m.lemmatize(text)
    return ' '.join([lemma for lemma in lemmas if lemma != ' '])


def parse_xml(file_name):
    with open(file_name) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        titles = soup.find_all('title')
        texts = soup.find_all('text')
        return [PageInfo(title=title.get_text(), text=text.get_text()) for title, text in zip(titles, texts)]


def process_line(line, valid_symbols_regexp='[^а-я ]'):
    return lemmatize(delete_stop_words(delete_extra_symbols(line.lower(), valid_symbols_regexp)))


def process_text(page_info_list):
    result = []
    for page_info in page_info_list:
        print(page_info)
        new_title = process_line(page_info.title)
        new_text = process_line(page_info.text)
        result.append(PageInfo(title=new_title, text=new_text))
    return result


def save_processed_text(page_info_list):
    with open('result.txt', 'w') as result_file:
        for page_info in page_info_list:
            result_file.write(page_info.title)
            result_file.write(f'{page_info.text}\n')


def main():
    page_info_list = parse_xml('../t01_web_crawler/result.xml')
    page_info_list = process_text(page_info_list)
    save_processed_text(page_info_list)


if __name__ == '__main__':
    main()

	
	# зачтено