import math
from collections import defaultdict

from t03_inverted_index_and_boolean_search.boolean_search import load_inverted_index, get_all_docs_ids
from t03_inverted_index_and_boolean_search.inverted_index import save_inverted_index


def get_word_counts_per_document(src_path):
    current_document_id = 1
    result = defaultdict(int)
    with open(src_path, 'r') as file:
        for line in file:
            if line != '\n':
                result[current_document_id] += len(line.split())
            else:
                current_document_id += 1
    return result


def get_inverted_index_with_tfidf(inverted_index, word_count_per_document):
    result = {}
    total_docs_count = len(get_all_docs_ids(inverted_index))
    for word, doc_list in inverted_index.items():
        new_doc_list = []
        idf = math.log(total_docs_count / len(doc_list))
        for doc_id, term_count_in_doc in doc_list:
            tf = term_count_in_doc / word_count_per_document[doc_id]
            new_doc_list.append((doc_id, term_count_in_doc, tf * idf, idf))
        result[word] = new_doc_list
    return result


def main():
    inverted_index = load_inverted_index('../t03_inverted_index_and_boolean_search/inverted_index_result.xml')
    word_count_per_document = get_word_counts_per_document('../t02_preprocessing/result.txt')
    inverted_index_with_tfidf = get_inverted_index_with_tfidf(inverted_index, word_count_per_document)
    save_inverted_index(inverted_index_with_tfidf,
                        'inverted_index_with_tfidf_extended_result.xml.j2',
                        'inverted_index_with_tfidf_extended_result.xml',
                        transform_internal_dicts_to_tuples=False)


if __name__ == '__main__':
    main()

	
	# 0 значит не релевантно. в выводе быть не должно.
	# зачтено