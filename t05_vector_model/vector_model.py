import math
import os
from collections import Counter

from bs4 import BeautifulSoup

from t02_preprocessing.preprocess import process_line
from t03_inverted_index_and_boolean_search.boolean_search import get_all_docs_ids


def normalize_vector(vector):
    return math.sqrt(sum([el * el for el in vector]))


def get_cos_between_vectors(query_vector, doc_vector):
    query_vector_vals = query_vector.values()
    doc_vector_vals = doc_vector.values()
    dot_product = sum([q * d for q, d in zip(query_vector_vals, doc_vector_vals)])
    return dot_product / (normalize_vector(query_vector_vals) * normalize_vector(doc_vector_vals))


def get_query_term_vector(query, inverted_index):
    result = dict.fromkeys(inverted_index.keys(), 0)
    counted_query_words_dict = Counter(query.split())
    query_words_total_count = len(query.split())
    for word, count in counted_query_words_dict.items():
        if word in result:
            tf = count / query_words_total_count
            idf = inverted_index[word][0][3]
            result[word] = tf * idf
    return result


def get_ranked_results(query, inverted_index, matrix, k=10):
    query_vector = get_query_term_vector(query, inverted_index)
    matrix_with_cos = [{'doc_vector': doc_vector, 'cos': get_cos_between_vectors(query_vector, doc_vector)}
                       for doc_vector in matrix]
    return [(i + 1, val['cos']) for i, val in sorted(enumerate(matrix_with_cos),
                                                     key=lambda el: el[1]['cos'],
                                                     reverse=True)[:k]]


def get_doc_term_vector(doc_id, inverted_index):
    result = dict.fromkeys(inverted_index.keys(), 0)
    for key, vals in inverted_index.items():
        for val in vals:
            if val[0] == doc_id:
                result[key] = val[2]
    return result


def get_doc_term_matrix(inverted_index):
    all_docs_ids = sorted(get_all_docs_ids(inverted_index))
    result = []
    for doc_id in all_docs_ids:
        result.append(get_doc_term_vector(doc_id, inverted_index))
    return result


def load_inverted_index_with_tfidf_extended(file_path):
    inverted_index = {}
    with open(file_path) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        term_list = soup.find_all('term')
        for term in term_list:
            doc_list = []
            for doc in term.find_all('doc'):
                doc_list.append([
                    int(doc['id']),
                    int(doc['count']),
                    float(doc['tf-idf']),
                    float(doc['idf'])
                ])
            inverted_index[term['value']] = doc_list
    return inverted_index


def get_urls_of_docs(xml_file_path, all_docs_ids):
    result = {}
    with open(xml_file_path) as xml_file:
        xml_file_content = xml_file.read()
        soup = BeautifulSoup(xml_file_content, 'lxml')
        for doc_id in all_docs_ids:
            docs = soup.find_all('document', {'id': doc_id})
            for doc in docs:
                result[doc_id] = doc.find_all('url')[0].text
    return result


def main():
    inverted_index_extended_path = 'inverted_index_with_tfidf_extended_result.xml'
    vector_result_path = 'vector_result.txt'
    xml_file_path = '../t01_web_crawler/result.xml'

    inverted_index_with_tfidf_and_idf = load_inverted_index_with_tfidf_extended(inverted_index_extended_path)
    matrix = get_doc_term_matrix(inverted_index_with_tfidf_and_idf)
    query_list = ['по базовому трафику в подсети',
                  'базовые станции билайна',
                  'безопасность современной версии браузера',
                  'контроль версий история',
                  'роскомнадзор разблокировал рутрекер',
                  'экзамен по информационному поиску']
    normalized_query_list = [process_line(query) for query in query_list]

    all_docs_ids = sorted(get_all_docs_ids(inverted_index_with_tfidf_and_idf))
    url_by_id_dict = get_urls_of_docs(xml_file_path, all_docs_ids)

    with open(vector_result_path, 'w') as vector_result_file:
        for query, normalized_query in zip(query_list, normalized_query_list):
            vector_result_file.write(query + os.linesep)
            res = get_ranked_results(normalized_query, inverted_index_with_tfidf_and_idf, matrix)
            for doc_id, cos in res:
                vector_result_file.write(f'{url_by_id_dict[doc_id]} - {cos}{os.linesep}')
            vector_result_file.write(os.linesep)


if __name__ == '__main__':
    main()
