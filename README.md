# intro-inf-retrieval
Репозиторий по курсу "Основы информационного поиска".

[Инструкция](../master/git-tutorial.md#дополнение) для объединения вашего и шаблонного репозиториев.


* * *

# Домашние задания

1. Написать веб-краулер, который парсит выбранный Вами тематический сайт.

Первый этап: выделение ссылок на статьи/посты.

Второй этап: выделение заголовка статьи, текста и указанные ключевые слова/теги/данные из мета-тега keywords. Если ключевые слова ни в каком виде не указаны, выберите другой сайт.

Текст на сайте должен быть преимущественно русский. Количество спарсенных страниц не менее 30.

Выделенные блоки не должны содержать тегов, скриптов, ссылок итп - только текст. Проблем с кодировкой тоже не должно быть.

Результат записать в виде xml-файла со следующей структурой и кодировкой UTF-8:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<documents>
	<document id = "1">
		<url/>
		<title/>
		<text/>
		<keywords/>
	</document>
	<document id = "2">
		<url/>
		<title/>
		<text/>
		<keywords/>
	</document>
</documents>
```

Язык программирования выбираете сами, можете использовать библиотеки. При выделении элементов html-документа можете использовать XPath, XQuery, а также другие инструменты рассматривающие html-документы в виде объекта.